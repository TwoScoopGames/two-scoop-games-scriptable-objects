using TwoScoopGames.ScriptableObjects.Events;
using UnityEngine;
using UnityEngine.Events;

namespace TwoScoopGames.ScriptableObjects.Events {
  public class TransformGameEventListener : GameEventListener<Transform> {
    public TransformGameEvent subject;
    public TransformUnityEvent response;

    protected override GameEvent<Transform> GetSubject() {
      return subject;
    }

    protected override UnityEvent<Transform> GetResponse() {
      return response;
    }
  }
}
