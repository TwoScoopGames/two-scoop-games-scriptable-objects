using System;
using UnityEngine.Events;

namespace TwoScoopGames.ScriptableObjects.Events {
  [Serializable]
  public class StringUnityEvent : UnityEvent<string> {}
}
