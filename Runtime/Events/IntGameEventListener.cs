using UnityEngine;
using UnityEngine.Events;

namespace TwoScoopGames.ScriptableObjects.Events {
  public class IntGameEventListener : GameEventListener<int> {
    public IntGameEvent subject;
    public IntUnityEvent response;

    protected override GameEvent<int> GetSubject() {
      return subject;
    }

    protected override UnityEvent<int> GetResponse() {
      return response;
    }
  }
}
