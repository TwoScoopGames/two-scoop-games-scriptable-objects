using System;
using UnityEngine;
using UnityEngine.Events;

namespace TwoScoopGames.ScriptableObjects.Events {
  [Serializable]
  public class TransformUnityEvent : UnityEvent<Transform> {}
}
