using UnityEngine;
using UnityEngine.Events;

namespace TwoScoopGames.ScriptableObjects.Events {
  public class StringGameEventListener : GameEventListener<string> {
    public StringGameEvent subject;
    public StringUnityEvent response;

    protected override GameEvent<string> GetSubject() {
      return subject;
    }

    protected override UnityEvent<string> GetResponse() {
      return response;
    }
  }
}
