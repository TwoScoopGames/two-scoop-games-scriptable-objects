using System.Collections.Generic;
using UnityEngine;

namespace TwoScoopGames.ScriptableObjects.Events {
  [CreateAssetMenu(menuName="Events/IntGameEvent")]
  public class IntGameEvent : GameEvent<int> {}
}
