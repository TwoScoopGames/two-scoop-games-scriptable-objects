using System.Collections.Generic;
using UnityEngine;

namespace TwoScoopGames.ScriptableObjects.Events {
  [CreateAssetMenu(menuName="Events/FloatGameEvent")]
  public class FloatGameEvent : GameEvent<float> {}
}
