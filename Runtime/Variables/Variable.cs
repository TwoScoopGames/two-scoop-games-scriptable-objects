﻿using System;
using UnityEngine;

namespace TwoScoopGames.ScriptableObjects.Variables {
  public class Variable<T> : ScriptableObject {

    public T InitialValue;

    [NonSerialized]
    public T RuntimeValue;

    [Multiline]
    [SerializeField]
    private string DeveloperDescription;

    void OnEnable() {
      RuntimeValue = InitialValue;
      this.hideFlags = HideFlags.DontUnloadUnusedAsset;
    }
  }
}
